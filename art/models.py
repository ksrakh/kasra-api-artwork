from django.db.models import CharField , TextField , CASCADE , IntegerField , BooleanField , ForeignKey , Model , ManyToManyField , ImageField
from django.contrib.auth.models import User



class Artcategory(Model):
    title = CharField(max_length=60)
    art_count = IntegerField(default=0)

    def __str__(self):
        return self.title


class Art(Model):
    name = CharField(max_length=100)
    price = CharField(max_length=30)
    exist = BooleanField(default=True)
    number = IntegerField(default=0)
    descriptions = TextField()
    image = ImageField(upload_to = 'art_image')
    related_user= ForeignKey(User , on_delete=CASCADE , related_name='art_related_user')
    related_arts_category = ForeignKey(Artcategory , on_delete=CASCADE)
    authors = ManyToManyField(User)

    def __str__(self):
        return self.name


    def save(self , *args , **kwargs):
        super(Art , self).save(*args, **kwargs)
        category = self.related_arts_category
        category.art_count = Art.objects.filter(related_arts_category=self.related_arts_category).count()
        category.save()


class Comment(Model):
    context = TextField()
    related_user = ForeignKey(User , on_delete=CASCADE)
    related_art = ForeignKey(Art , on_delete=CASCADE)


    def __str__(self):
        return self.context


class Images(Model):
    images = ImageField(upload_to = 'art_image')
    realted_art= ForeignKey(Art , on_delete=CASCADE)

    def __str__(self):
        return self.images.url

