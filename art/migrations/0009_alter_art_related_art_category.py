# Generated by Django 4.0.4 on 2022-06-27 13:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('art', '0008_alter_art_related_art_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='art',
            name='related_art_category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='art.artcategory'),
        ),
    ]
