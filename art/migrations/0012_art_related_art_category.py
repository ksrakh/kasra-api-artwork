# Generated by Django 4.0.4 on 2022-06-27 15:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('art', '0011_alter_art_related_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='art',
            name='related_art_category',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='art.artcategory'),
            preserve_default=False,
        ),
    ]
