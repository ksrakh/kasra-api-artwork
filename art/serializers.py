from rest_framework.serializers import ModelSerializer , SerializerMethodField
from django.contrib.auth.models import User
from .models import *

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        exclude = ['password']

class UserMiniSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name' , 'last_name']

class ArtCategorySerializer(ModelSerializer):
    class Meta:
        model = Artcategory
        fields = '__all__'


class ArtSerializer(ModelSerializer):
    related_arts_category = ArtCategorySerializer(read_only=True)
    related_user = UserMiniSerializer(read_only=True)
    authors = UserMiniSerializer(many=True  ,read_only=True)
    comments_count = SerializerMethodField()
    class Meta:
        model = Art
        fields = '__all__'


    def get_comments_count(self , obj):
        return obj.comment_set.all().count()

class ArtFormSerializer(ModelSerializer):
    class Meta:
        model = Art
        fields = '__all__'

class ArtMiniSerializer(ModelSerializer):
    class Meta:
        model = Art
        fields = ['id' , 'name']


class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'

class ImageSerializer(ModelSerializer):
    class Meta:
        model = Images
        fields = '__all__'