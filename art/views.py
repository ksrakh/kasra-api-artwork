from django.contrib.auth.models import User
from rest_framework.viewsets import ModelViewSet
from .permissions import AuthorAllStaffAllButEditOrReadOnly
from .serializers import *
from rest_framework.permissions import IsAuthenticatedOrReadOnly , IsAuthenticated
from django.core.cache import cache
from rest_framework.parsers import MultiPartParser , FormParser


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    def get_serializer_class(self):
        if self.action == 'list':
            return UserMiniSerializer
        return UserSerializer


    permission_classes_by_action = {'create': [IsAuthenticated],
                                    'list': []}
    
    filterset_fields = ['username', 'first_name' , 'last_name']
    search_fields = ['username', 'first_name' , 'last_name']
    ordering_fields = ['username' , 'email']

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            return [permission() for permission in self.permission_classes]



class ArtCategoryViewSet(ModelViewSet):
    queryset = Artcategory.objects.all()
    serializer_class = ArtCategorySerializer


class ArtViewset(ModelViewSet):
    queryset = Art.objects.select_related('related_user').all()

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrive':
            if self.request.user.is_superuser:
                return ArtSerializer
            return ArtMiniSerializer
        return ArtFormSerializer

    permission_classes = [AuthorAllStaffAllButEditOrReadOnly]
    parser_classes = [MultiPartParser , FormParser]


class CommentViewset(ModelViewSet):
    def get_queryset(self):
        if cache.get('queryset'):
            queryset = cache.get('queryset')
        else:
            queryset = Comment.objects.all()
            cache.set('queryset' , queryset , 60)
        return queryset

    serializer_class = CommentSerializer


class ImageViewset(ModelViewSet):
    queryset = Images.objects.all()
    serializer_class = ImageSerializer
    parser_classes = [MultiPartParser , FormParser]