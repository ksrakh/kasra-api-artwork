from django.contrib.admin import site , ModelAdmin , StackedInline
from art.models import *

class CommentInline(StackedInline):
    model = Comment

class ImageInline(StackedInline):
    model = Images

class ArtAdmin(ModelAdmin):
    list_display = ['id' ,'name','exist','related_user' ]
    inlines = [
        CommentInline ,
        ImageInline

    ]


site.register(Art , ArtAdmin)
